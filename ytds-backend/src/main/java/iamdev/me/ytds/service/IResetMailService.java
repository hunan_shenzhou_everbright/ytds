package iamdev.me.ytds.service;

import iamdev.me.ytds.entity.ResetMail;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
public interface IResetMailService extends IService<ResetMail> {

}

package iamdev.me.ytds.config;

import com.netflix.spectator.gc.GcLogger;
import iamdev.me.ytds.utils.DateUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.util.Date;

/**
 *
 * Title: BaseConfig.java
 *
 * @author zxc
 * @time 2018/5/10 下午4:54
 */
@Configuration
@ConfigurationProperties(prefix = "base")
@EnableConfigurationProperties(BaseConfig.class)
public class BaseConfig {

    @PostConstruct
    public void init(){
        File file=new File(uploadDir);
        if(!file.exists()){
            file.mkdirs();
        }
    }


    private String uploadDir;
    private String sofficePath;


    public String getSofficePath() {
        return sofficePath;
    }

    public void setSofficePath(String sofficePath) {
        this.sofficePath = sofficePath;
    }

    public String getUploadDir() {
        return uploadDir;
    }

    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }


    public File getTodayUploadDir(){
        String today=DateUtils.formatDate1(new Date());
        File t=new File(uploadDir,today);
        if(!t.exists()){
            t.mkdirs();
        }
        return t;
    }
}

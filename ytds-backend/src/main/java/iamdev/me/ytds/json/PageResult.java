package iamdev.me.ytds.json;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import java.util.List;

/**
 * Created by zxc on 2017/9/12.
 */
public class PageResult<T> {
    public List<T> list;
    public Pagination page;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public Pagination getPage() {
        return page;
    }

    public void setPage(Pagination page) {
        this.page = page;
    }
}

package iamdev.me.ytds.config;

import iamdev.me.ytds.interceptor.LoginInterceptor;
import iamdev.me.ytds.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *
 * Title: YtdsInterceptorConfig.java
 *
 * @author zxc
 * @time 2018/6/24 下午7:17
 */
@Configuration
public class YtdsInterceptorConfig implements WebMvcConfigurer {

    @Autowired
    private IUserService userService;
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
    增加登录拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry){
        logger.info("addInterceptors:{}","LoginInterceptor");
        InterceptorRegistration loginInterceptor = registry.addInterceptor(new LoginInterceptor(userService));
        loginInterceptor.addPathPatterns("/**");
    }


}

package iamdev.me.ytds.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@TableName("tb_doc_draft")
public class DocDraft implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 草稿文档id
     */
    @TableId("draft_doc_id")
    private Integer draftDocId;
    /**
     * 草稿保存时间
     */
    @TableField("draft_save_date")
    private Date draftSaveDate;
    /**
     * 草稿文档内容
     */
    @TableField("draft_doc_content")
    private String draftDocContent;
    /**
     * 草稿文档标题
     */
    @TableField("draft_doc_title")
    private String draftDocTitle;


    public Integer getDraftDocId() {
        return draftDocId;
    }

    public void setDraftDocId(Integer draftDocId) {
        this.draftDocId = draftDocId;
    }

    public Date getDraftSaveDate() {
        return draftSaveDate;
    }

    public void setDraftSaveDate(Date draftSaveDate) {
        this.draftSaveDate = draftSaveDate;
    }

    public String getDraftDocContent() {
        return draftDocContent;
    }

    public void setDraftDocContent(String draftDocContent) {
        this.draftDocContent = draftDocContent;
    }

    public String getDraftDocTitle() {
        return draftDocTitle;
    }

    public void setDraftDocTitle(String draftDocTitle) {
        this.draftDocTitle = draftDocTitle;
    }

    @Override
    public String toString() {
        return "DocDraft{" +
        ", draftDocId=" + draftDocId +
        ", draftSaveDate=" + draftSaveDate +
        ", draftDocContent=" + draftDocContent +
        ", draftDocTitle=" + draftDocTitle +
        "}";
    }
}

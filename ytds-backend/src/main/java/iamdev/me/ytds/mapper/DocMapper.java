package iamdev.me.ytds.mapper;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import iamdev.me.ytds.entity.Doc;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
public interface DocMapper extends BaseMapper<Doc> {

    List<Doc> findDoc(Pagination pagination, Map<String,Object> params);
    Map queryDocTypeNumber(@Param("docUserId")Integer docUserId);
}

package iamdev.me.ytds.service.impl;

import iamdev.me.ytds.config.BaseConfig;
import iamdev.me.ytds.service.ISofficeService;
import iamdev.me.ytds.utils.OfficeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 *
 * Title: SofficeServiceImpl.java
 *
 * @author zxc
 * @time 2018/6/29 下午5:03
 */
@Service
public class SofficeServiceImpl implements ISofficeService {
    @Autowired
    private BaseConfig baseConfig;

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    public boolean convertPdf(File srcFile,File targetFile){

        return OfficeUtils.convertPdf(baseConfig.getSofficePath(),srcFile,targetFile);
    }
}

package iamdev.me.ytds.service;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import iamdev.me.ytds.entity.EvernoteImport;
import com.baomidou.mybatisplus.service.IService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxc
 * @since 2018-07-04
 */
public interface IEvernoteImportService extends IService<EvernoteImport> {

    @Transactional
    boolean importEverNote(MultipartFile file, Integer userId);

    @Transactional
    void importEverNoteSync(Integer importId);

    List<EvernoteImport> queryImportRecord(Pagination pagination, Integer userId);
}

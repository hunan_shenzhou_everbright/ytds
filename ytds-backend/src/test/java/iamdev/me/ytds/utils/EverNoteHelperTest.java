package iamdev.me.ytds.utils;

import com.alibaba.fastjson.JSON;
import com.netflix.spectator.gc.GcEvent;
import com.netflix.spectator.gc.GcEventListener;
import com.netflix.spectator.gc.GcLogger;
import iamdev.me.ytds.evernote.EverNoteHelper;
import org.junit.Test;

import java.io.File;

/**
 *
 * Title: EverNoteUtilsTest.java
 *
 * @author zxc
 * @time 2018/7/3 下午2:07
 */
public class EverNoteHelperTest {


    @Test
    public void test() throws InterruptedException {
        EverNoteHelper.parseEverNoteFile(new File("/Users/zxc/Desktop/文档/我的笔记.enex"),everNote -> {
            System.out.println(JSON.toJSONString(everNote,true));
        });
    }

    @Test
    public void test2() throws InterruptedException {
        EverNoteHelper.parseEverNoteFile(new File("/Users/zxc/Desktop/文档/部分笔记.enex"),everNote -> {
            System.out.println(JSON.toJSONString(everNote,true));
        });
    }
}

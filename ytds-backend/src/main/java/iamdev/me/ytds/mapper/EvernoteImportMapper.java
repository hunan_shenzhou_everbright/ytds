package iamdev.me.ytds.mapper;

import iamdev.me.ytds.entity.EvernoteImport;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zxc
 * @since 2018-07-04
 */
public interface EvernoteImportMapper extends BaseMapper<EvernoteImport> {

}

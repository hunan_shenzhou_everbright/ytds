package iamdev.me.ytds.service.impl;

import iamdev.me.ytds.entity.DocRecycle;
import iamdev.me.ytds.mapper.DocRecycleMapper;
import iamdev.me.ytds.service.IDocRecycleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@Service
public class DocRecycleServiceImpl extends ServiceImpl<DocRecycleMapper, DocRecycle> implements IDocRecycleService {

}

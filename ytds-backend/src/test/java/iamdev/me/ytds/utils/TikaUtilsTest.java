package iamdev.me.ytds.utils;

import org.apache.commons.io.FileUtils;
import org.apache.tika.detect.EncodingDetector;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.txt.UniversalEncodingDetector;
import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Title: TikaUtilsTest.java
 * @author zxc
 * @time 2018/5/10 22:39
 */


public class TikaUtilsTest {
    @Test
    public void test(){
        System.out.println(TikaUtils.parseContent(new File("/Users/zxc/project/springboot/lindex/upload/2018-06-28/d3ccef62a8674935b46032bfb407641apdf.pdf")));
    }

    @Test
    public void test2(){
        InputStream stream = ClassPathUtils.getInputStream("/test.txt");
        System.out.println(TikaUtils.parseContent(stream));
    }

}

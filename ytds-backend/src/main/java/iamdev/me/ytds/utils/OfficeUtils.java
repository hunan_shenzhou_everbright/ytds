package iamdev.me.ytds.utils;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * Title: OfficeUtils.java
 *
 * @author zxc
 * @time 2018/6/29 下午5:20
 */
public class OfficeUtils {

    private static Logger logger = LoggerFactory.getLogger(OfficeUtils.class);

    public static boolean convertPdf2(String sofficePath,File srcFile, File targetDir){
        ///Applications/LibreOffice.app/Contents/MacOS/soffice
        // --headless --invisible --convert-to
        // html --outdir pdf-html/ *.pdf

        Runtime runtime =Runtime.getRuntime();
        StringBuilder command = new StringBuilder();
        command.append(sofficePath);
        String outputDir = targetDir.getAbsolutePath();
        command.append(" --headless --invisible --convert-to pdf --outdir ");
        command.append(outputDir);
        command.append(" ");
        command.append(srcFile.getAbsolutePath());
        logger.info("开始转换pdf,命令行是:{}",command.toString());
        try {

            ProcessBuilder pb = new ProcessBuilder(command.toString());
            pb.redirectErrorStream(true);
            Process process = pb.start();
            printStream(process.getInputStream());
            //Process process = runtime.exec(command.toString());
//            printStream(process.getInputStream());
//            printStream(process.getErrorStream());
            int code = process.waitFor();
            logger.info("code:{}",code);

            //String result = IOUtils.toString(process.getInputStream(),"utf-8");
            //String errorResult = IOUtils.toString(process.getErrorStream(),"utf-8");
            //logger.info("convertPdf from {} output {}:\n result:{}\n",srcFile.getAbsolutePath(),outputDir,result);
//            if(StringUtil.isNotEmpty(errorResult)){
//                logger.error("convertPdf from {} output {}:\n errorResult:{}\n",srcFile.getAbsolutePath(),outputDir,errorResult);
//            }
            File targetFile  = new File(targetDir,StringUtils.getFileNameWithoutSuffix(srcFile.getName())+".pdf");
            logger.info("目标文件是:{}",targetFile.getAbsolutePath());
            if(targetFile.exists()){
                logger.info("文档转化成功");
                return true;
            }else {
                logger.info("文档转化失败");
                return false;
            }

//            if(code==0){
//                logger.info("文档转化成功");
//                return true;
//            }else {
//                logger.info("文档转化失败");
//                return false;
//            }
        } catch (IOException e) {
            logger.error("convertPdf",e);
        } catch (InterruptedException e) {
            logger.error("convertPdf",e);
        }
        return false;
    }

    public static boolean convertPdf(String sofficePath,File srcFile, File targetDir){
        ///Applications/LibreOffice.app/Contents/MacOS/soffice
        // --headless --invisible --convert-to
        // html --outdir pdf-html/ *.pdf
        //Thread.sleep();
        List<String>commands=new ArrayList<>();
        String outputDir = targetDir.getAbsolutePath();
        commands.add(sofficePath);
        commands.add("--headless");
        commands.add("--invisible");
        commands.add("--convert-to");
        commands.add("pdf");
        commands.add("--outdir");
        commands.add(outputDir);
        commands.add(srcFile.getAbsolutePath());
        logger.info("开始转换pdf,命令行是:{}", Arrays.toString(commands.toArray()));
        try {

            ProcessBuilder pb = new ProcessBuilder(commands);
            pb.redirectErrorStream(true);
            Process process = pb.start();
            String result = IOUtils.toString(process.getInputStream(),"utf-8");
            logger.info("convertPdf from {} output {}:\n result:{}\n",srcFile.getAbsolutePath(),outputDir,result);
            //printStream(process.getInputStream());
            //Process process = runtime.exec(command.toString());
//            printStream(process.getInputStream());
//            printStream(process.getErrorStream());
            //int code = process.waitFor();
            //logger.info("code:{}",code);
            process.destroy();

            //String result = IOUtils.toString(process.getInputStream(),"utf-8");
            //String errorResult = IOUtils.toString(process.getErrorStream(),"utf-8");
            //logger.info("convertPdf from {} output {}:\n result:{}\n",srcFile.getAbsolutePath(),outputDir,result);
//            if(StringUtil.isNotEmpty(errorResult)){
//                logger.error("convertPdf from {} output {}:\n errorResult:{}\n",srcFile.getAbsolutePath(),outputDir,errorResult);
//            }
            File targetFile  = new File(targetDir,StringUtils.getFileNameWithoutSuffix(srcFile.getName())+".pdf");
            logger.info("目标文件是:{}",targetFile.getAbsolutePath());
            if(targetFile.exists()){
                logger.info("文档转化成功");
                return true;
            }else {
                logger.info("文档转化失败");
                return false;
            }

//            if(code==0){
//                logger.info("文档转化成功");
//                return true;
//            }else {
//                logger.info("文档转化失败");
//                return false;
//            }
        } catch (IOException e) {
            logger.error("convertPdf",e);
        }
        return false;
    }

    public static void printStream(InputStream inputStream){
        new Thread(){
            @Override
            public void run() {
                logger.info("开始读取流,thread:{}",currentThread().getId());
                try {
                    String s = IOUtils.toString(inputStream,"utf-8");
                    logger.info("流内容:{}",s);
                } catch (IOException e) {
                    logger.error("读取流异常",e);
                }
                logger.info("结束读取流,thread:{}",currentThread().getId());
            }
        }.start();
    }


}

package iamdev.me.ytds.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@TableName("tb_register_mail")
public class RegisterMail implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 注册id
     */
    @TableId(value = "register_id", type = IdType.AUTO)
    private Integer registerId;
    /**
     * 注册邮箱
     */
    private String email;
    /**
     * 注册代码
     */
    private String code;
    /**
     * 发送时间
     */
    @TableField("send_date")
    private Date sendDate;




    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    @Override
    public String toString() {
        return "RegisterMail{" +
        ", registerId=" + registerId +
        ", email=" + email +
        ", code=" + code +
        ", sendDate=" + sendDate +
        "}";
    }
}

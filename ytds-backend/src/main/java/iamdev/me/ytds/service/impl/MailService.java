package iamdev.me.ytds.service.impl;

import iamdev.me.ytds.service.IMailService;
import jodd.mail.Email;
import jodd.mail.MailServer;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

/**
 *
 * Title: MailService.java
 *
 * @author zxc
 * @time 2018/6/24 下午1:01
 */
@Service
@Configuration
public class MailService implements IMailService {
    @Value("${mail.smtp}")
    private String smtp;
    @Value("${mail.name}")
    private String name;
    @Value("${mail.password}")
    private String password;
    @Override
    public void sendMail(Email email){
        SmtpServer smtpServer = MailServer.create()
                .ssl(true)
                .host(smtp)
                .auth(name, password)
                .buildSmtpMailServer();
        SendMailSession session = smtpServer.createSession();
        session.open();
        session.sendMail(email);
        session.close();
    }
    @Override
    public void sendMail(String title,String content,String to){
        SmtpServer smtpServer = MailServer.create()
                .ssl(true)
                .host(smtp)
                .auth(name, password)
                .buildSmtpMailServer();
        SendMailSession session = smtpServer.createSession();
        session.open();
        Email email = Email.create()
                .from(name)
                .to(to)
                .subject(title)
                .textMessage(content);
        session.sendMail(email);
        session.close();
    }
}

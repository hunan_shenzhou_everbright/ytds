package iamdev.me.ytds.service.impl;

import iamdev.me.ytds.entity.RegisterMail;
import iamdev.me.ytds.mapper.RegisterMailMapper;
import iamdev.me.ytds.service.IRegisterMailService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@Service
public class RegisterMailServiceImpl extends ServiceImpl<RegisterMailMapper, RegisterMail> implements IRegisterMailService {

}

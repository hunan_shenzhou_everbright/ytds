package iamdev.me.ytds.service.impl;

import iamdev.me.ytds.entity.ShareDoc;
import iamdev.me.ytds.mapper.ShareDocMapper;
import iamdev.me.ytds.service.IShareDocService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@Service
public class ShareDocServiceImpl extends ServiceImpl<ShareDocMapper, ShareDoc> implements IShareDocService {

}
